# Tugas 1 - Pengemabangan Aplikasi Web dengan TDD, Django, Models, HTML, CSS
***
CSGE602022 - Web Design & Programming (Perancangan & Pemrograman Web) @
Faculty of Computer Science Universitas Indonesia, Odd Semester 2017/2018

[![pipeline status](https://gitlab.com/TP-PaPoW/TP-PPW/badges/master/pipeline.svg)](https://gitlab.com/TP-PaPoW/TP-PPW/commits/master)
[![coverage report](https://gitlab.com/TP-PaPoW/TP-PPW/badges/master/coverage.svg)](https://gitlab.com/TP-PaPoW/TP-PPW/commits/master)
***
#### TP-PaPoW
Anggota:
- Hamam Wulan Ayu - 1606875762
- Kemala Andiyani - 1606836862
- Misael Jonathan - 1606879773
- Muhammad Imbang Murtito - 1606889502

App Link: http://tp-papow.herokuapp.com/

